<?php


require_once "model/frontend.php";
require_once "model/manager.php";

function listPosts()
{
    $frontend = new Frontend();
    $posts = $frontend->getPosts();
    require_once "view/listPostsView.php";
}

function post()
{
    $frontend = new Frontend();
    $post = $frontend->getPost($_GET['id']);
    $comments = $frontend->getComments($_GET['id']);
    // var_dump($post);exit;
    require_once "view/postView.php";
}

function addComment($postId, $author, $comment)
{
    $frontend = new frontend();
    $postComment = $frontend->postComment($postId, $author, $comment);
    $affectedLines = $frontend->postComment($postId, $author, $comment);

    if ($affectedLines === false) {
        die('Impossible d\'ajouter le commentaire !');
    }
    else {
        header('Location: index.php?action=post&id=' . $postId);
    }
}

function Comment($id)
{
    $frontend = new frontend();
    $comment = $frontend->getComment($_GET['commentId']);
    $getPost = $frontend->getPost($getComment['post_id']);

    // $getComment = getComment($_GET['commentId']);
    // $getPost = getPost($getComment['post_id']);

    require_once "view/getCommentView.php";
}

function updateComment($commentId, $author, $comment)
{
    $frontend = new frontend();
    $udapteComment = $frontend->udpComment();
    $updatedLines = $frontend->updComment($commentId, $author, $comment);

    if ($updatedLines === false) {
        die('Impossible de modifier le commentaire !');
    }
    else {
        header('Location: index.php?action=post&id='.$_POST['post_id']);
    }
}