<?php
require_once "model/frontend.php";
require_once "model/backend.php";

// function listPosts()
// {
//     $posts = getPosts();
//
//     require('view/listPostsView.php');
// }

// function post()
// {
//    $post = getPost($_GET['id']);
//    $comments = getComments($_GET['id']);

//    require('view/postView.php');
// }

function adminView()
{
    $admin = new Frontend();
    $posts = $admin->getPosts();
    $comments = $admin->getComments();
    var_dump($comments);exit;
    require_once "view/adminPageView.php";
}

function addPost($postId, $author, $content, $title)
{
    $postAdd = new backend();
    $affectedPost = postAdd($postId, $author, $content, $title);

    if ($affectedPost === false) {
        die('Impossible d\'ajouter le commentaire !');
    }
    else {
        header('Location: index.php?action=post&id=' . $postId);
    }
}

function updatePost($postId, $author, $content, $title)
{
    $updPost = new backend();
    $updatedPost = updPost($postId, $author, $content, $title);

    if ($updatedPost === false) {
        die('Impossible de modifier le commentaire !');
    }
    else {
        header('Location: index.php?action=post&id='.$_POST['post_id']);
    }
}


function editTwinPost($commentId, $author, $comment, $postId)
{
    $twinPost = new backend();
    $editTwinPost = twinPost($commentId, $author, $comment, $postId);

    if ($twinPost === false) {
        die('Impossible de modifier le chapitre !');
    }
    else {
        header('Location: index.php?action=post&id='.$_POST['post_id']);
    }
}

function deletePost($commentId,$postId, $author, $comment, $content, $title)
{
    $dltPost = new backend();
    $deletePost = dltPost($commentId, $postId, $author,$comment, $content, $title);

    if ($deletePost === false) {
        die('Impossible de modifier le commentaire !');
    }
    else {
        header('Location: index.php?action=post&id='.$_POST['post_id']);
    }
}
