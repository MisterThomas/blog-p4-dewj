<?php $title = 'Mon blog'; ?>

<?php require('view/header.php'); ?>
<?php
    while ($data = $posts->fetch())
    {
?>
        <div class="news">
            <h3>
                <?= htmlspecialchars($data['title']); ?>
                <em>le <?= $data['creation_date_fr']; ?></em>
            </h3>
            
            <p>
            <?php
            // On affiche le contenu du billet
            echo nl2br(htmlspecialchars($data['content']));
            ?>
            <br />
            <em><?= '<a href="index.php?action=post&amp;id='.$data['id'].'">Commentaires</a>';?></em>
            </p>
        </div>
<?php
} // Fin de la boucle des billets
$posts->closeCursor();
?>

<?php require('view/footer.php'); ?>