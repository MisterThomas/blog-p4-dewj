<?php $title = 'Mon blog'; ?>

<?php 
require('view/header.php'); ?>
<p><a href="./index.php" class=>Retour à la liste des Articles</a></p>
        <div class="news">
            <h3>
            <!-- <?php var_dump($post);?> -->

                <?= htmlspecialchars($post['title']) ?>
                <em>le <?= $post['creation_date_fr'] ?></em>
            </h3>
            
            <p>
                <?= nl2br(htmlspecialchars($post['content'])) ?>
            </p>
        </div>

        <h2 class="commentaire">acceder au commentaire</h2>

        <form action="index.php?action=addComment&amp;id=<?= $post['id'] ?>" method="post">
            <div>
                <label for="author">Auteur</label><br />
                <input type="text" id="author" name="author" />
            </div>
            <div>
                <label for="comment">Commentaire</label><br />
                <textarea id="comment" name="comment"></textarea>
            </div>
            <textarea id="tinymce" name="tinymce"></textarea>
            <div>
                <input type="submit" />
            </div>
        </form>

        <?php
        while ($comment = $comments->fetch())
        {
        ?>
            <p><strong><?= htmlspecialchars($comment['author']) ?></strong> le <?= $comment['comment_date_fr'] ?> <em><a href="index.php?action=Comment&amp;commentId=<?= $comment['id'] ?>">Modifier</a></em></p>
            <p><?= nl2br(htmlspecialchars($comment['comment'])) ?></p>
        <?php
        }
$comments->closeCursor();
?>

<?php 
require('view/footer.php'); ?>