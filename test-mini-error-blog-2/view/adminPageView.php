<section id="adminPage">
	<article id="WritPost">

		<h2><span class="maj">é</span>crire un nouveau chapitre:</h2>

			<form id="getNewPost" action="./index.php?action=post" method="post">
					
				<label>Titre:<input type="text" name="title" id="title" value="" required/></label>
					
				<textarea class="tinymce" name="tinymce_post"></textarea>
					
				<input type="submit" id="send" value="Publier" />
			</form>
		</article>

		<aside id="gestionBlock">

			<div id="CommAdmin">
				<h2> Liste des commentaires à vérifier:</h2>
					<?php
						while ($listComments= $comments->fetch()) {
					?>
					<p class="warning"><strong>Message:</strong> <?= nl2br($listComments['content']);?> <br/>
						Ecrit par: <?php echo htmlspecialchars($listComments['author']);?> le: <?php echo htmlspecialchars($listComments['comment_date']);?></br>
						
						<span id="deleteButton">
							<a href="./index.php?action=deleteComment&amp;id=<?php echo $listComments['commentId']; ?>">Supprimer</a>
						</span>
						<span id="okButton">
							<a href="./index.php?action=UpdateComment&amp;id=<?php echo $listComments['commentId']; ?>">Valider</a>
						</span>
					</p>

					<?php
						}
							$Comments-> closeCursor();
					?>
					
			</div>
				<div id="line_Ink"><!--Ink line add in css--></div>
			<div id="postAdmin">
				<h2> Liste des chapitres déjà publié:</h2>
					<?php
						while ($list=$listposts->fetch() ) {
					?>
					<p class="warning">
						<?php echo $list['title']?>
						</br>
						<span id="deletePost">
							<a href="./index.php?action=deletePost&amp;id=<?php echo $list['id']; ?>"> Effacer</a>
						</span>
						<span id="changeButton">
							<a href="./index.php?action=editPost&amp;id=<?php echo $list['id']; ?>">Modifier</a>
						</span>

						 <br/>
						</p>
						<?php
							}
							$listPosts->closeCursor();
						?>
						
			</div>
		</aside>

</section>

</body>
</html>