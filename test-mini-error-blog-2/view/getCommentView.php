<?php $title = 'Mon blog'; ?>
<?php 
require('view/header.php'); ?>
<h1>Mon super blog !</h1>
    <p><a href="./index.php?action=post&id=<?= $getComment['post_id'] ?>">Retour au post</a></p>
        <div class="news">
            <h3>
                <?= htmlspecialchars($getPost['title']) ?>
                <em>le <?= $getPost['creation_date_fr'] ?></em>
            </h3>
            
            <p>
                <?= nl2br(htmlspecialchars($getPost['content'])) ?>
            </p>
        </div>
        <h2>Modification du commentaire</h2>

       <form action="index.php?action=updateComment&amp;commentId=<?= $getComment['id'] ?>" method="post">
            <div>
                <label for="author">Auteur</label><br />
                <input type="text" id="author" name="author" value="<?= $getComment['author'] ?>" />
            </div>
            <div>
                <label for="comment">Commentaire</label><br />
                <textarea id="comment" name="comment"><?= $getComment['comment'] ?></textarea>
            </div>
            <div>
                <input type="hidden" name="post_id" value="<?= $getComment['post_id'] ?>"/>
                <input type="submit" />
            </div>
        </form>
<?php 
require('view/footer.php'); ?>