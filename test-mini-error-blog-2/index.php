<?php
require('controller/frontend.php');
require('controller/backend.php');

/*--------------------------------COMMENTS et LISTS----------------------------------------*/

if (isset($_GET['action'])) 
{
    if ($_GET['action'] == 'listPosts') 
    {
        listPosts();
    }
    elseif ($_GET['action'] == 'post') 
    {
        if (isset($_GET['id']) && $_GET['id'] > 0) 
        {
            post();
        }
        else 
        {
            echo 'Erreur : aucun identifiant de billet envoyé';
        }
    }
    elseif ($_GET['action'] == 'addComment') 
    {
        if (isset($_GET['id']) && $_GET['id'] > 0) 
        {
            if (!empty($_POST['author']) && !empty($_POST['comment'])) 
            {
                addComment($_GET['id'], $_POST['author'], $_POST['comment']);
            }
            else 
            {
                echo 'Erreur : tous les champs ne sont pas remplis !';
            }
        }
    }
    elseif ($_GET['action'] == 'Comment') 
    {
        if (isset($_GET['commentId']) && $_GET['commentId'] > 0) 
        {
            Comment($_GET['commentId']);
        }
    }
    elseif ($_GET['action'] == 'updateComment') 
    {
        if (isset($_GET['commentId']) && $_GET['commentId'] > 0) 
        {
            updateComment($_GET['commentId'], $_POST['author'], $_POST['comment']);
        }
    }
        elseif ($_GET['action'] == 'warningComment') 
        {
            if (isset($_GET['commentId']) && $_GET['commentId'] > 0) 
            {
                warningComment($_GET['commentId'], $_POST['author'], $_POST['comment']);
            }
        }
}
else 
{
    listPosts();
}

/*--------------------------------FIN COMMENTS ET LISTS----------------------------------------*/

/*--------------------------------ARTICLES----------------------------------------*/

if (isset($_GET['action'])) 
{
    if ($_GET['action'] == 'listPosts') 
    {
        listPosts();
    }
    elseif ($_GET['action'] == 'post') 
    {
        if (isset($_GET['id']) && $_GET['id'] > 0) 
        {
            post();
        }
        else 
        {
            echo 'Erreur : aucun identifiant de chapitre envoyé';
        }
    }
    elseif ($_GET['action'] == 'admin')
    {
        adminView();
    }
    elseif ($_GET['action'] == 'addpost') 
    {
        if (isset($_GET['id']) && $_GET['id'] > 0) 
        {
            if (!empty($_POST['author']) && !empty($_POST['post'])) 
            {
                addPost($_GET['id'], $_POST['author'], $_POST['post']);
            }
            else 
            {
                echo 'Erreur : aucun identifiant de chapitre ajoute ';
            }
        }
    }
    elseif ($_GET['action'] == 'EditPost') 
    {
        if (isset($_GET['commentId']) && $_GET['commentId'] > 0) 
        {
            Comment($_GET['commentId']);
        }
    }
    elseif ($_GET['action'] == 'TwinymcePost') 
    {
        if (isset($_GET['postId']) && $_GET['postId'] > 0) 
        {
            updatePost($_GET['postId'], $_POST['author'], $_POST['post']);
        }
    }
    elseif ($_GET['action'] == 'deletePost') 
    {
        if (isset($_GET['postId']) && $_GET['postId'] > 0) 
        {
            deleteArticle($_GET['postId'], $_POST['author'], $_POST['post']);
        }
        else 
        {
            echo 'Erreur : le chapitre nest pas supprimer ';
        }
    }
}
else 
{
    listPosts();
}


/*--------------------------------FIN ARTICLES----------------------------------------*/