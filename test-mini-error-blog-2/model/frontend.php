<?php
 require_once "./model/manager.php";
class Frontend {

function getPosts()
{
	$manager = new Manager();
    $bdd = $manager->dbConnect();
	$posts = $bdd->query('SELECT id, title, content, DATE_FORMAT(creation_date, \'%d/%m/%Y à %Hh%imin%ss\') AS creation_date_fr FROM posts ORDER BY creation_date DESC LIMIT 0, 5');

	 return $posts;
}

function getPost($postId)
{
    $manager = new Manager();
    $bdd = $manager->dbConnect();
    $req = $bdd->prepare('SELECT id, title, content, DATE_FORMAT(creation_date, \'%d/%m/%Y à %Hh%imin%ss\') AS creation_date_fr FROM posts WHERE id = ?');
    $req->execute(array($postId));
    $post = $req->fetch();

    return $post;
}

function getComments($postId)
{
    $manager = new Manager();
    $bdd = $manager->dbConnect();
    $comments = $bdd->prepare('SELECT id, author, comment, DATE_FORMAT(comment_date, \'%d/%m/%Y à %Hh%imin%ss\') AS comment_date_fr FROM comments WHERE post_id = ? ORDER BY comment_date DESC');
    $comments->execute(array($postId));

    return $comments;
}


function getComment($commentId)
{
    $manager = new Manager();
    $bdd = $manager->dbConnect();
    $req = $bdd->prepare('SELECT id, post_id, author, comment, DATE_FORMAT(comment_date, \'%d/%m/%Y à %Hh%imin%ss\') AS comment_date_fr FROM comments WHERE id = ?');
    $req->execute(array($commentId));
    $comment = $req->fetch();

    return $comment;
}

function postComment($postId, $author, $comment)
{
    $manager = new Manager();
    $bdd = $manager->dbConnect();
    $comments = $bdd->prepare('INSERT INTO comments(post_id, author, comment, comment_date) VALUES(?, ?, ?, NOW())');
    $affectedLines = $comments->execute(array($postId, $author, $comment));

    return $affectedLines;
}

function updComment($commentId, $author, $comment)
{
    $manager = new Manager();
    $bdd = $manager->dbConnect();
    $update_comment = $bdd->prepare('UPDATE comments SET author=:author, comment=:comment, comment_date=NOW() WHERE id=:commentId');
    $affectedComment = $update_comment->execute(array('author'=>$author, 'comment'=>$comment, 'commentId'=>$commentId ));

    return $affectedComment;
}

function wrgComment ($commentId, $author, $comment)
{
    $manager = new Manager();
    $bdd = $manager->dbConnect();
    $warning_comment = $bdd->prepare('SELECT comments(post_id, author, comment, comment_date)VALUES(?, ?, ?, WARNING())');
    $affectedWargComment = $comments->execute(array('postId'=>$postId, 'author'=>$author, 'comment'=>$comment, 'commentId'=>$commentId));

    return $affectedWargComment;
}
}